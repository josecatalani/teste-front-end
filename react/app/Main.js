import React from 'react';
import PropTypes from 'prop-types';

import Detail from './components/Detail';
import Result from './components/Result';
import Searchbar from './components/Searchbar';

const KEY = 'AIzaSyBTo8mOJjxoqYSXEkdIZQdz07ldQndgKaA';
const LIMIT = 12;

class Main extends React.Component {
	constructor(props){
		super(props);

		this.state = {
			videos: [],
			search: "",
			showDetail: false,
			selectedVideo: {},
			selectedVideoId: '',
			fullMenuView: true,
			nextPageToken: '',
			prevPageToken: '',
			closedDetails: false
		};

		this.onChange = this.onChange.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
		this.toggleMenuView = this.toggleMenuView.bind(this);
		this.toggleDetail = this.toggleDetail.bind(this);
		this.onSelectedVideo = this.onSelectedVideo.bind(this);
		this.getSelectedVideo = this.getSelectedVideo.bind(this);
		this.makeSearch = this.makeSearch.bind(this);
		this.prevPagination = this.prevPagination.bind(this);
		this.nextPagination = this.nextPagination.bind(this);
	}
	scrollToTop	() {
		$("body, html").animate({scrollTop: 0}, 300, "swing");
	}
	makeSearch(pageToken = null){
		var _self = this;
		var url_search = 'https://www.googleapis.com/youtube/v3/search';
		var searchParams = {
	 		part: 'id,snippet',
		 	q: this.state.search,
		 	key: KEY,
		 	maxResults: LIMIT,
		 	type: 'video'
		};

		if(pageToken){
			searchParams.pageToken = pageToken;
		}

		axios.get(url_search, {
			params: searchParams
		})
		.then(function(response) {
			var videos_response = [];
			response.data.items.forEach(function(item){
				if(item.id.videoId) {
					videos_response.push({
						id : !item.id.videoId ? 1 : item.id.videoId,
						title : item.snippet.title,
						description : item.snippet.description,
						thumbnail : item.snippet.thumbnails.high.url
					});
				}
			});

			var inner_next_page_token = "";
			var inner_prev_page_token = "";

			if(response.data.nextPageToken) {
				inner_next_page_token = response.data.nextPageToken;
			}

			if(response.data.prevPageToken) {
				inner_prev_page_token = response.data.prevPageToken;
			}

			_self.scrollToTop();

			_self.setState({ 
				loading: false, 
				videos:videos_response, 
				nextPageToken: inner_next_page_token, 
				prevPageToken: inner_prev_page_token
			});
		})
		.catch(function(err){
			console.log(err);
		});
	}
	prevPagination (){
		this.makeSearch(this.state.prevPageToken);
	}
	nextPagination (){
		this.makeSearch(this.state.nextPageToken);
	}
	toggleMenuView(){
		if(!document.body.classList.contains('detail')) {
			document.body.classList.toggle('fixed');
			this.setState({ fullMenuView: !this.state.fullMenuView })
		} else {
			this.toggleDetail()
		}
	}
	toggleDetail(){
		document.body.classList.toggle('detail');
		this.setState({ showDetail: !this.state.showDetail, closedDetails: true });
	}
	onSelectedVideo(videoId){
		this.setState({ selectedVideoId: videoId }, () =>{ this.getSelectedVideo() });
	}
	getSelectedVideo(){
		var _self = this;

		this.setState({ loading: true });

		var url_search = 'https://www.googleapis.com/youtube/v3/videos';
		var searchParams = {
	 		part: 'snippet,statistics',
		 	id: this.state.selectedVideoId,
		 	key: KEY
		};

		axios.get(url_search, {
			params: searchParams
		})
		.then(function(response) {
			var response_obj = response.data.items[0];

			var selected_video = {
				id: response_obj.id,
				title: response_obj.snippet.title,
				description: response_obj.snippet.description,
				views: response_obj.statistics.viewCount
			};

			_self.scrollToTop();
			_self.setState({ loading: false, selectedVideo: selected_video });
			_self.toggleDetail();
		})
		.catch(function(err){
			console.log(err);
		});
	}
	onChange(event) {
		this.setState({ search: event.target.value })
	}
	onSubmit(event) {
		var _self = this;

		if(this.state.fullMenuView) {
			this.toggleMenuView();
		}

		if(this.state.showDetail){
			this.toggleDetail();
		}

		this.setState({ loading: true });

		event.preventDefault();

		this.makeSearch(null);
	}
	render() {
		return (
			<div>
				<Searchbar  
					toggleDetail={this.toggleDetail}
					showDetail={this.state.showDetail} 
					search={this.state.search} 
					onChange={this.onChange} 
					onSubmit={this.onSubmit} 
					onLogoClick={this.toggleMenuView}
					/>
				<Detail 
					closedDetails={this.state.closedDetails}
					showDetail={this.state.showDetail} 
					video={this.state.selectedVideo} 
					toggleDetail={this.toggleDetail}
					/>

				<Result
					showDetail={this.state.showDetail} 
					videos={this.state.videos} 
					onSelectedVideo={this.onSelectedVideo}
					nextPageToken={this.state.nextPageToken}
					prevPageToken={this.state.prevPageToken}
					nextPagination={this.nextPagination}
					prevPagination={this.prevPagination}
					/>
			</div>
		)
	}
}

Main.propTypes = {
}

export default Main;