import React from 'react';
import PropTypes from 'prop-types';
import Searchbar from './Searchbar';
import Pagination from './Pagination';
import List from './List';
import queryString from 'query-string';
import {HashRouter as Router, Route, Switch, hashHistory, history} from 'react-router-dom';

class Result extends React.Component {
	constructor(props){
		super(props);
	}
	render() {
		return (
			<div>
				<List
					items={this.props.videos} 
					onSelectedVideo={this.props.onSelectedVideo}
					showDetail={this.props.showDetail}
					/>
				<Pagination
					showDetail={this.props.showDetail}
					nextPageToken={this.props.nextPageToken}
					prevPageToken={this.props.prevPageToken}
					nextPagination={this.props.nextPagination}
					prevPagination={this.props.prevPagination}
					/>
			</div>
		)
	}
}

Result.propTypes = {
}

Result.defaultProps = {
	videos: []
}

export default Result;