import React from 'react';
import PropTypes from 'prop-types';
import ListItem from './ListItem';

class List extends React.Component {
	constructor(props){
		super(props);
	}
	render() {
		var items = this.props.items;
		return (
		<section id="search-result" className={this.props.showDetail ? "hide-list" : "show-list"} >
			<div className="container">
				<div className="row">
					<div className="search-result-list">
						{items.map((video, index) => 
			              <ListItem key={video.id}
						    	id={video.id}
						    	thumbnail={video.thumbnail}
						    	title={video.title}
						    	description={video.description}
						    	onSelectedVideo={this.props.onSelectedVideo}
					    	/>
				    	)}
					</div>
				</div>
			</div>
		</section>
		)
	}
}

List.propTypes = {
}

export default List;