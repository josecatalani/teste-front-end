import React from 'react';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';

class Back extends React.Component {
	constructor(props){
		super(props);
	}
	render() {
		return (
        	<div className="back-btn" onClick={() => {this.props.toggleDetail()}}>voltar</div>
		)
	}
}

Back.propTypes = {
}

export default Back;