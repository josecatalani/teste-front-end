import React from 'react';
import PropTypes from 'prop-types';
import Back from './Back.js';
import { Redirect, Link } from 'react-router-dom';

class Searchbar extends React.Component {
	render() {
		return (
	 	<nav id="searchbar">
        	<div className="container">
      			<div className="row">
        			<div className="logo-wrapper">
              			<img id="main-logo" src="img/geral/madeinweb.png" onClick={this.props.onLogoClick} />
                    <Back toggleDetail={this.props.toggleDetail}/>
            		</div>
            		<div className="search-input-wrapper">
              			<form>
                			<input id="search-area" type="text" placeholder="Pesquisar" onChange={this.props.onChange} value={this.props.search} />
                			<button id="submit-search" type="submit" onClick={this.props.onSubmit}>Buscar</button>
              			</form>
            		</div>
      			</div>
        	</div>
      	</nav>
		)
	}
}

Searchbar.propTypes = {
	search: PropTypes.string,
	onChange: PropTypes.func,
	onSubmit: PropTypes.func
}

export default Searchbar;