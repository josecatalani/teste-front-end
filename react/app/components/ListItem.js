import React from 'react';
import PropTypes from 'prop-types';
import { Redirect, Link } from 'react-router-dom';

class ListItem extends React.Component {
	constructor(props){
		super(props);
	}
	render() {
		return (

		<div className="search-result-item" onClick={() => {this.props.onSelectedVideo(this.props.id)}}>
    		<div className="search-result-image">
          		<img src={this.props.thumbnail} alt={this.props.title} />
        	</div>
    		<div className="search-result-description">
          		<p className="search-result-title">{this.props.title}</p>
          		<p className="search-result-text">{this.props.description}</p>
        	</div>
      	</div>
		)
	}
}

ListItem.propTypes = {
	id : PropTypes.string,
	thumbnail : PropTypes.string,
	title : PropTypes.string,
	description : PropTypes.string
}

export default ListItem;