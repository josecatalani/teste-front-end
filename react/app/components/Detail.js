import React from 'react';
import PropTypes from 'prop-types';
import Searchbar from './Searchbar';
import Video from './Video';

class Detail extends React.Component {
	constructor(props){
		super(props);
	}
	render() {
		return (
			<Video 
				closedDetails={this.props.closedDetails} 
				showDetail={this.props.showDetail} 
				id={this.props.video.id}
				title={this.props.video.title}
				description={this.props.video.description}
				views={this.props.video.views}
				toggleDetail={this.props.toggleDetail}
				/>
		)
	}
}

Detail.propTypes = {
	id : PropTypes.string,
}

export default Detail;

