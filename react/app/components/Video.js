import React from 'react';
import PropTypes from 'prop-types';
import Back from './Back.js';
import YouTube from 'react-youtube';

class Video extends React.Component {
	constructor(props){
		super(props);

    this.state = {
      player: [],
      isReady: false
    }
    this._onReady = this._onReady.bind(this);
	}
  _onReady(event) {
    const player = this.state.player;
    player.push(event.target);
      this.setState({
      player: player,
      isReady: true
    });
  }
  componentDidUpdate(prevProps, prevState) {
    if(prevProps.closedDetails){
      this.state.player[0].pauseVideo();
    }
  }
	render() {
    const opts = {
      width: '100%',
      height: '100%',
      playerVars:{
        autoPlay: 1
      }
    }
		return (
      <section id="video-detail" className={this.props.showDetail ? "show-detail" : "hide-detail"}>
        <div id="video-display">
          <div className="container">
            <div className="row">
              <div id="video-area">
                <Back toggleDetail={this.props.toggleDetail}/>
                <div id="embed">
                  <YouTube
                    videoId={this.props.id}
                    onReady={this._onReady}
                    opts={opts}
                    />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="container">
          <div className="row">
            <div className="video-details">
              <h2 className="video-title">{this.props.title}</h2>
              <p className="count-views">{this.props.views} visualizações</p>
              <p className="video-description">{this.props.description}</p>
            </div>
          </div>
        </div>
      </section>
      )
	}
}

Video.propTypes = {
	id : PropTypes.string,
	thumbnail : PropTypes.string,
	title : PropTypes.string,
	description : PropTypes.string,
	views: PropTypes.string,
	showDetail: PropTypes.bool
}

export default Video;