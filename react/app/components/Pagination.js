import React from 'react';
import PropTypes from 'prop-types';

class Pagination extends React.Component {
	constructor(props){
		super(props);
	}
	render() {
		let next = this.props.nextPageToken;
		let prev = this.props.prevPageToken;

		let nextBtn = null;
		let prevBtn = null;

		if(next) {
			nextBtn = <button id="next-btn" className="pagination-btn" onClick={() => {this.props.nextPagination()}}>Próximo</button>
		}
		if(prev) {
			prevBtn = <button id="prev-btn" className="pagination-btn" onClick={() => {this.props.prevPagination()}}>Anterior</button>
		}

		return (
			<div>
				<div className="container">
					<div className="row">
						<div className={this.props.showDetail ? "hide pagination-wrapper" : "show pagination-wrapper"} >
							{nextBtn}
							{prevBtn}
						</div>
					</div>
				</div>
			</div>
		)
	}
}

Pagination.propTypes = {
}

export default Pagination;