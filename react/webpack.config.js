module.exports = {
    entry: [
        './app/App.js',
    ],
    output: {
        path: __dirname + '/public/js', 
        filename: 'bundle.js'
    },
    devServer: {
        inline: true,
        contentBase: __dirname + '/public',
        port: 3333
    },
    module: {
        loaders: [{
            test: /\.jsx?$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
            query: {
                presets: ['es2015', 'react']
            }
        }]
    },
    devServer: { 
        inline: true 
    }
}