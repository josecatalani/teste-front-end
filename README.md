# Teste Madeinweb: Front-End
Candidato: José Victor Catalani
Aplicação Reactjs

## Entrega
- Temos duas pastas: html/ e react/, a primeira é apenas os htmls puros, e segunda é o projeto em si.
- Utilizei Reactjs com webpack para empacotar.. Todo o projeto pós build está dentro de react/public, abrindo o index.html é possível rodar o projeto.
- Para rodar o ambiente de DEV, é necessário rodar 'npm run dev' e 'npm run watch', o webpack irá rodar um ambiente em http://localhost:8080/public/#/